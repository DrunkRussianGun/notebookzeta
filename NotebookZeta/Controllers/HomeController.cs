﻿using Microsoft.AspNetCore.Mvc;
using NotebookZeta.Models;
using System.Diagnostics;
using NotebookZeta.DataAccess;

namespace NotebookZeta.Controllers
{
	[Route("[controller]/[action]")]
	public class HomeController : Controller
	{
        private readonly UnitOfWork unitOfWork;

		public HomeController(UnitOfWork unitOfWork)
		{
			this.unitOfWork = unitOfWork;
		}

		[HttpGet]
		public IActionResult Index()
		{
            var notes = unitOfWork.GetAllNotes();
			return View(new IndexViewModel { NotesList = notes });
		}

		[HttpGet]
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
