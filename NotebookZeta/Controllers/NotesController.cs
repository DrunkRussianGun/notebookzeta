﻿using Microsoft.AspNetCore.Mvc;
using NotebookZeta.Models;
using NotebookZeta.DataAccess;
using NotebookZeta.Core;
using System;
using System.Linq;
using System.Collections.Generic;

namespace NotebookZeta.Controllers
{
	[Route("[controller]/[action]")]
	public class NotesController : Controller
	{
        private readonly UnitOfWork unitOfWork;

		public NotesController(UnitOfWork unitOfWork)
		{
            this.unitOfWork = unitOfWork;
		}

		[HttpGet]
		[Route("{id}")]
		public IActionResult Get(int id)
		{
			var note = unitOfWork.GetNote(id);
			if (note == null)
				return NotFound();

			return View(note);
		}
		
		[HttpGet]
		[Route("{id}")]
		public IActionResult GetAttachment(int id)
		{
			var file = unitOfWork.GetAttachment(id);
			if (file == null)
				return NotFound();

			return File(file.Content, file.ContentType, file.FileName);
		}

		[HttpPost]
		public IActionResult Add(NoteDto noteDto)
		{
            var note = new Note
            {
                Title = noteDto.Title,
                Text = CastHelper.CastText(noteDto.Text),
                CreationTime = DateTime.Now
            };
            note.Attachments = noteDto.Attachments?
                .Select(file => CastHelper.CastFormFile(file, note))
                .ToList() ?? new List<AttachedFile>();
            
            unitOfWork.AddNote(note);
			return RedirectToAction("Index", "Home");
		}

		[HttpPost]
		[Route("{id}")]
		public IActionResult Remove(int id)
		{
			unitOfWork.RemoveNote(id);
			return RedirectToAction("Index", "Home");
		}
	}
}