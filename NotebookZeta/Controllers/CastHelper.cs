﻿using Microsoft.AspNetCore.Http;
using NotebookZeta.Core;
using System;

namespace NotebookZeta.Controllers
{
    public static class CastHelper
    {
        public static string[] CastText(string text)
        {
            return text.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static AttachedFile CastFormFile(IFormFile file, Note note)
        {
            var bytes = new byte[file.Length];
            file.OpenReadStream().Read(bytes);
            return new AttachedFile
            {
                FileName = file.FileName,
                ContentType = file.ContentType,
                Content = bytes,
                Note = note
            };
        }
    }
}
