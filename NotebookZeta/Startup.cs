﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using NotebookZeta.Core;
using NotebookZeta.DataAccess;

namespace NotebookZeta
{
	public class Startup
	{
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<ApplicationContext>();
            services.AddScoped<UnitOfWork>();

            services.AddScoped<Repository<Note>>();
            services.AddScoped<Repository<AttachedFile>>();

            services.AddSingleton(new MemoryCache<Note>(x => x.Id));
            services.AddSingleton(new MemoryCache<AttachedFile>(x => x.Id));

            services.AddSwaggerGen(options =>
            {
	            options.SwaggerDoc("swagger_doc",
		            new OpenApiInfo()
		            {
						Title = "NewTextDocument",
						Version = "1.0"
		            });
            });
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
			else app.UseExceptionHandler("/Home/Error");

			app.UseStaticFiles();

			app.UseSwagger();
			app.UseSwaggerUI(options =>
			{
				options.SwaggerEndpoint("/swagger/swagger_doc/swagger.json", "NewTextDocument");
				options.RoutePrefix = string.Empty;
			});
			
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					 name: "default",
					 template: "{controller=Home}/{action=Index}");
				routes.MapSpaFallbackRoute(
					name: "spa-fallback",
					defaults: new { controller = "Home", action = "Index" });
			});
		}
	}
}
