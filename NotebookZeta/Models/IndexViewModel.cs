﻿using System.Collections.Generic;
using NotebookZeta.Core;

namespace NotebookZeta.Models
{
	public class IndexViewModel
	{
		public IEnumerable<Note> NotesList { get; set; }
	}
}
