﻿using Microsoft.AspNetCore.Http;

namespace NotebookZeta.Models
{
    public class NoteDto
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public IFormFileCollection Attachments { get; set; }
	}
}
