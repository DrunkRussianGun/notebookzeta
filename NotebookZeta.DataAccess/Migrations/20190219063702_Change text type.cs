﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NotebookZeta.DataAccess.Migrations
{
    public partial class Changetexttype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Text",
                table: "Notes");
            migrationBuilder.AddColumn<string[]>(
                name: "Text",
                table: "Notes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Text",
                table: "Notes");
            migrationBuilder.AddColumn<string>(
                name: "Text",
                table: "Notes",
                nullable: true);
        }
    }
}
