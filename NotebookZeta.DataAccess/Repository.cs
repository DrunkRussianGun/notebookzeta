﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NotebookZeta.DataAccess
{
    public class Repository<TEntity>
        where TEntity : class
    {
        public Repository(ApplicationContext applicationContext, MemoryCache<TEntity> cache = null)
        {
            this.applicationContext = applicationContext;
            entities = applicationContext.Set<TEntity>();

            this.cache = cache;
        }

        public TEntity Get(int id)
        {
            if (cache != null && cache.TryGet(id, out var entity))
                return entity;

            var result = entities.Find(id);
            cache?.Add(result);
            return result;
        }

        public List<TEntity> Get(Func<TEntity, bool> predicate)
        {
            var result = entities
                .Where(x => predicate(x))
                .AsNoTracking()
                .ToList();
            cache?.Add(result);
            return result;
        }

        public List<TEntity> GetAll()
        {
            var result = entities
                .AsNoTracking()
                .ToList();
            cache?.Add(result);
            return result;
        }

        public void Add(TEntity entity)
        {
            entities.Add(entity);
            cache?.Add(entity);
        }

        public void Remove(int id)
        {
            var entity = Get(id);
            if (entity != null)
            {
                entities.Remove(entity);
                cache?.Remove(id);
            }
        }

        private readonly ApplicationContext applicationContext;
        private readonly DbSet<TEntity> entities;
        private readonly MemoryCache<TEntity> cache;
    }
}
