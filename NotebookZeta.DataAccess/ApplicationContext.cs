﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NotebookZeta.Core;

namespace NotebookZeta.DataAccess
{
    public class ApplicationContext : DbContext
    {
        private readonly string connectionString;

        public ApplicationContext(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
            //Database.Migrate();
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options) { }

        public DbSet<Note> Notes { get; private set; }

        public DbSet<AttachedFile> AttachedFiles { get; private set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AttachedFile>()
                .HasOne<Note>()
                .WithMany(note => note.Attachments)
                .HasForeignKey(file => file.NoteId)
                .OnDelete(DeleteBehavior.Cascade);

            base.OnModelCreating(modelBuilder);
        }
    }
}
