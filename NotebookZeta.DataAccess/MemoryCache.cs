﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace NotebookZeta.DataAccess
{
    public class MemoryCache<TCachedEntity>
        where TCachedEntity : class
    {
        public TimeSpan RecordTimeToLive { get; set; } = TimeSpan.FromMinutes(1);

        public MemoryCache(Func<TCachedEntity, int> idGetter)
        {
            this.idGetter = idGetter ?? throw new ArgumentNullException(nameof(idGetter));
        }

        public bool IsAvailable(int id)
        {
            RemoveIfOutdated(id);
            return cache.ContainsKey(id);
        }

        public TCachedEntity Get(int id)
        {
            RemoveIfOutdated(id);
            if (cache.TryGetValue(id, out var record))
                return record.Entity;

            return default(TCachedEntity);
        }

        public bool TryGet(int id, out TCachedEntity entity)
        {
            RemoveIfOutdated(id);

            var result = cache.TryGetValue(id, out var record);
            entity = record.Entity;
            return result;
        }

        public void Add(TCachedEntity entity)
        {
            var id = idGetter(entity);
            cache[id] = new Record(entity, stopwatch.Elapsed);
        }

        public void Add(IEnumerable<TCachedEntity> entities)
        {
            foreach (var entity in entities)
                Add(entity);
        }

        public void Remove(int id)
        {
            cache.Remove(id);
        }

        private struct Record
        {
            public readonly TCachedEntity Entity;
            public readonly TimeSpan TimeOfCreation;

            public Record(TCachedEntity entity, TimeSpan timeOfCreation)
            {
                Entity = entity;
                TimeOfCreation = timeOfCreation;
            }
        }

        private readonly Func<TCachedEntity, int> idGetter;

        private readonly Dictionary<int, Record> cache = new Dictionary<int, Record>();
        private readonly Stopwatch stopwatch = Stopwatch.StartNew();

        private void RemoveIfOutdated(int id)
        {
            if (!cache.TryGetValue(id, out var record))
                return;

            var recordAge = stopwatch.Elapsed - record.TimeOfCreation;
            if (recordAge > RecordTimeToLive)
                cache.Remove(id);
        }
    }
}
