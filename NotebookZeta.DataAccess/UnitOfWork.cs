﻿using NotebookZeta.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NotebookZeta.DataAccess
{
    public class UnitOfWork : IDisposable
    {
        public UnitOfWork(
            ApplicationContext applicationContext,
            Repository<Note> notesRepository,
            Repository<AttachedFile> filesRepository)
        {
            this.applicationContext = applicationContext;

            this.notesRepository = notesRepository;
            this.filesRepository = filesRepository;
        }

        public IEnumerable<Note> GetAllNotes()
        {
            return notesRepository.GetAll();
        }

        public Note GetNote(int id)
        {
            var note = notesRepository.Get(id);
            note.Attachments = filesRepository
                .Get(x => x.NoteId == id)
                .ToList();
            return note;
        }

        public void AddNote(Note note)
        {
            notesRepository.Add(note);
            applicationContext.SaveChanges();
        }

        public void RemoveNote(int id)
        {
            notesRepository.Remove(id);
            applicationContext.SaveChanges();
        }

        public AttachedFile GetAttachment(int id)
        {
            return filesRepository.Get(id);
        }

        public void Dispose()
        {
            applicationContext.Dispose();
        }

        private readonly ApplicationContext applicationContext;

        private readonly Repository<Note> notesRepository;
        private readonly Repository<AttachedFile> filesRepository;
    }
}
