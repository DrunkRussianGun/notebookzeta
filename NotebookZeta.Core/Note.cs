﻿using System;
using System.Collections.Generic;

namespace NotebookZeta.Core
{
	public class Note
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public DateTime CreationTime { get; set; }
		public string[] Text { get; set; }

		public List<AttachedFile> Attachments { get; set; }
	}
}
