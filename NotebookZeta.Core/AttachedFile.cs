﻿namespace NotebookZeta.Core
{
    public class AttachedFile
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }

        public int NoteId { get; set; }
        public Note Note { get; set; }
    }
}
